import {Component, OnInit} from '@angular/core';
import * as fromApp from 'src/app/state/app.reducer';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {BookDetail} from './books/models';
import {selectBookChapters} from 'src/app/state/app.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ng-ebook-store';
  chapters$: Observable<BookDetail[]>;

  constructor(
    private store: Store<fromApp.State>
  ) {

  }

  ngOnInit(): void {
    this.chapters$ = this.store.pipe(select(selectBookChapters));
  }
}
