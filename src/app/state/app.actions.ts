import {Action} from '@ngrx/store';
import {BookDetail, BookPageRequest} from '../books/models';

export enum AppActionTypes {
  APP_CHAPTER_REQUEST = '[APP] BOOK_CHAPTER_REQUEST',
  APP_CHAPTER_SUCCESS = '[APP] BOOK_CHAPTER_SUCCESS',
  APP_CHAPTER_FAILURE = '[APP] BOOK_CHAPTER_FAILURE'
}

export class GetBookChapter implements Action {
  readonly type = AppActionTypes.APP_CHAPTER_REQUEST;

  constructor(public payload: BookPageRequest) {
  }
}

export class GetBookChapterSuccess implements Action {
  readonly type = AppActionTypes.APP_CHAPTER_SUCCESS;

  constructor(public payload: BookDetail[]) {
  }
}

export class GetBookChapterFailure implements Action {
  readonly type = AppActionTypes.APP_CHAPTER_FAILURE;

  constructor(public payload: string) {
  }
}

export type AppActions = GetBookChapter
  | GetBookChapterSuccess
  | GetBookChapterFailure;
