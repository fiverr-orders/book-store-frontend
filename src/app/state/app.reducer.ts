import {AppActions, AppActionTypes} from './app.actions';
import {Book, BookDetail} from '../books/models';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {BookState} from '../books/state';

export interface State {
  search: any;
  chapters: ChapterState;
}

export interface ChapterState {
  error: string;
  chapters: BookDetail[];
}
const initialState: ChapterState = {
  chapters: [],
  error: ''
};

const getChapterState = createFeatureSelector<ChapterState>('chapters');

export const selectBookChapters = createSelector(
  getChapterState,
  (state: ChapterState) => state.chapters
);

export function reducer(state = initialState, action: AppActions): ChapterState {
  switch (action.type) {
    case AppActionTypes.APP_CHAPTER_REQUEST:
      return {
        ...state
      };

    case AppActionTypes.APP_CHAPTER_SUCCESS:
      const chapters = [...action.payload];

      const returnState = {
        ...state,
        chapters,
        error: ''
      };

      console.log(returnState);
      return returnState;

    case AppActionTypes.APP_CHAPTER_FAILURE:
      return {
        ...state,
        chapters: [],
        error: action.payload
      };


    default:
      return state;
  }
}
