export class Book {
  id: number;
  title: string;
  language: string;
  source: string;
  author: string;
  number_of_pages: string;
  published_date: string;
  published_location: string;
  publisher: string;
  publication_info: string;
  print_source: string;
  right_permissions: string;
  subject_terms: string
}
