import {Book} from './book.model';

export class BookDetail {
  id: number;
  page_number: number;
  section_number: number;
  chapter_title: string;
  physical_description: string;
  image: Image;
  book: Book;
  total_pages: number;
}

export class Image {
  url: string;
}
