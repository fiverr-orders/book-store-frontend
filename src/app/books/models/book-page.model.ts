export class BookPageRequest {
  page_number: number;
  book_id: number;
}

export class BookSearchRequest {
  search_term: string;
  search_source: boolean;
  search_author: boolean;
  search_publisher: boolean;
  search_published_date: boolean;
  search_published_location: boolean;
  search_publication_info: boolean;
}
