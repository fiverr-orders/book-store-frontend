import {Component, OnInit} from '@angular/core';
import {Observable, combineLatest} from 'rxjs';
import {Store, select} from '@ngrx/store';
import * as fromBook from '../../state/books.reducer';
import {ActivatedRoute, Router} from '@angular/router';
import {GetBookDetail} from '../../state';
import {map} from 'rxjs/operators';
import {BookDetail, BookPageRequest} from '../../models';
import {PageEvent} from '@angular/material/paginator';
import {GetBookChapter} from '../../../state/app.actions';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {
  book: BookDetail;
  errorMessage$: Observable<string>;

  constructor(
    private store: Store<fromBook.State>,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.store.pipe(select(fromBook.getBookPage)).subscribe(
      response => this.book = response
    );
    this.errorMessage$ = this.store.pipe(select(fromBook.getError));
  }

  ngOnInit(): void {
    combineLatest([
      this.route.paramMap,
      this.route.queryParamMap
    ]).pipe(
      map(combined => {
        const payload: BookPageRequest = {
          book_id: parseInt(combined[0].get('book_id'), 10),
          page_number: parseInt(combined[1].get('page_number'), 10)
        };

        return payload;
      })
    ).subscribe(payload => {
      this.store.dispatch(new GetBookDetail(payload));
      this.store.dispatch(new GetBookChapter(payload));
    });
  }

  async pageChanged(pageEvent: PageEvent) {
    await this.router.navigate(['/books', this.book.book.id], { queryParams: { page_number: pageEvent.pageIndex + 1 } });
  }
}
