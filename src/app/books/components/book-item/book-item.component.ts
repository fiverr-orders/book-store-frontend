import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../../models';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.scss']
})
export class BookItemComponent {

  @Input() book: Book;
  @Output() bookDetails: EventEmitter<any> = new EventEmitter();

  constructor() { }
}
