import {Component, OnInit} from '@angular/core';
import * as fromBook from '../../state/books.reducer';
import {Observable} from 'rxjs';
import {Book} from '../../models';
import {select, Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {getBooks} from '../../state/books.reducer';
import {getError} from '../../state/books.reducer';
import {GetBooks, SearchBooks} from '../../state';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

export interface Section {
  name: string;
  updated: Date;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  books: Book[];
  errorMessage$: Observable<string>;
  searchForm: FormGroup;

  constructor(
    private store: Store<fromBook.State>,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.store.pipe(select(getBooks)).subscribe(result => {
      this.books = result;
    });
    this.errorMessage$ = this.store.pipe(select(getError));

    this.searchForm = this.fb.group({
      search_term: new FormControl('', [Validators.required]),
      search_author: new FormControl(false),
      search_source: new FormControl(false),
      search_publisher: new FormControl(false),
      search_published_date: new FormControl(false),
      search_published_location: new FormControl(false),
      search_publication_info: new FormControl(false)
    });
  }

  searchBooks() {
    if (this.searchForm.dirty && this.searchForm.valid) {
      this.store.dispatch(new SearchBooks({
        search_term: this.searchForm.value.search_term,
        search_source: this.searchForm.value.search_source,
        search_author: this.searchForm.value.search_author,
        search_publisher: this.searchForm.value.search_publisher,
        search_published_date: this.searchForm.value.search_published_date,
        search_published_location: this.searchForm.value.search_published_location,
        search_publication_info: this.searchForm.value.search_publication_info
      }));
    }
  }

  get search_term() {
    return this.searchForm.get('search_term');
  }
}
