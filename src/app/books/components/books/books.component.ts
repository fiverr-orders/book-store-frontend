import {Component, OnInit} from '@angular/core';
import * as fromBook from '../../state/books.reducer';
import {getBooks, getError, GetBooks} from '../../state';
import {Store, select} from '@ngrx/store';
import {Observable} from 'rxjs';
import {Book} from '../../models';
import {Router} from '@angular/router';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  books$: Observable<Book[]>;
  errorMessage$: Observable<string>;

  constructor(
    private store: Store<fromBook.State>,
    private router: Router
  ) {
    this.books$ = this.store.pipe(select(getBooks));
    this.errorMessage$ = this.store.pipe(select(getError));
  }

  ngOnInit(): void {
    this.store.dispatch(new GetBooks());
  }

  async goToDetails(book: Book) {
    await this.router.navigate(['/books', book.id], {queryParams: {page_number: 1}});
  }
}
