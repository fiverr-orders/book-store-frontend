import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Book, BookDetail} from '../models';
import * as fromRoot from 'src/app/state/app.reducer';
import {BookActions, BookActionTypes} from './books.actions';

export interface State extends fromRoot.State {
  books: BookState;
}

export interface BookState {
  books: Book[];
  bookDetail: BookDetail;
  error: string;
  chapters: BookDetail[];
}

const initialState: BookState = {
  books: [],
  bookDetail: new BookDetail(),
  error: '',
  chapters: []
};

const getBooksFeatureState = createFeatureSelector<BookState>('books');

export const getBooks = createSelector(
  getBooksFeatureState,
  state => state.books
);

export const getError = createSelector(
  getBooksFeatureState,
  state => state.error
);

export const getBookPage = createSelector(
  getBooksFeatureState,
  state => state.bookDetail
);

export function reducer(state = initialState, action: BookActions): BookState {
  switch (action.type) {
    case BookActionTypes.BOOK_LIST_REQUEST:
    case BookActionTypes.BOOK_SEARCH_REQUEST:
    case BookActionTypes.BOOK_DETAIL_REQUEST:
      return {
        ...state
      };

    case BookActionTypes.BOOK_LIST_SUCCESS:
      return {
        ...state,
        books: action.payload,
        error: ''
      };

    case BookActionTypes.BOOK_LIST_FAILURE:
      return {
        ...state,
        books: [],
        error: action.payload
      };

    case BookActionTypes.BOOK_DETAIL_SUCCESS:
      return {
        ...state,
        bookDetail: action.payload,
        error: ''
      };

    case BookActionTypes.BOOK_DETAIL_FAILURE:
      return {
        ...state,
        bookDetail: new BookDetail(),
        error: action.payload
      };

    default:
      return state;
  }
}
