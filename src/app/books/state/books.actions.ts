import {Action} from '@ngrx/store';
import {Book, BookDetail, BookPageRequest, BookSearchRequest} from '../models';

export enum BookActionTypes {
  BOOK_LIST_REQUEST = '[BOOKS] BOOK_LIST_REQUEST',
  BOOK_LIST_FAILURE = '[BOOKS] BOOK_LIST_FAILURE',
  BOOK_LIST_SUCCESS = '[BOOKS] BOOK_LIST_SUCCESS',
  BOOK_DETAIL_REQUEST = '[BOOKS] BOOK_DETAIL_REQUEST',
  BOOK_DETAIL_SUCCESS = '[BOOKS] BOOK_DETAIL_SUCCESS',
  BOOK_DETAIL_FAILURE = '[BOOKS] BOOK_DETAIL_FAILURE',
  BOOK_SEARCH_REQUEST = '[BOOKS] BOOK_SEARCH_REQUEST',
}

export class GetBooks implements Action {
  readonly type = BookActionTypes.BOOK_LIST_REQUEST;
}

export class GetBooksSuccess implements Action {
  readonly type = BookActionTypes.BOOK_LIST_SUCCESS;

  constructor(public payload: Book[]) {
  }
}

export class GetBooksFailure implements Action {
  readonly type = BookActionTypes.BOOK_LIST_FAILURE;

  constructor(public payload: string) {
  }
}

export class GetBookDetail implements Action {
  readonly type = BookActionTypes.BOOK_DETAIL_REQUEST;

  constructor(public payload: BookPageRequest) {
  }
}

export class GetBookDetailSuccess implements Action {
  readonly type = BookActionTypes.BOOK_DETAIL_SUCCESS;

  constructor(public payload: BookDetail) {
  }
}

export class GetBookDetailFailure implements Action {
  readonly type = BookActionTypes.BOOK_DETAIL_FAILURE;

  constructor(public payload: string) {
  }
}

export class SearchBooks implements Action {
  readonly type = BookActionTypes.BOOK_SEARCH_REQUEST;

  constructor(public payload: BookSearchRequest) {
  }
}


export type BookActions = GetBooks
  | GetBooksSuccess
  | GetBooksFailure
  | GetBookDetail
  | GetBookDetailSuccess
  | GetBookDetailFailure
  | SearchBooks;
