import {Injectable} from '@angular/core';
import {Actions, ofType, Effect} from '@ngrx/effects';
import {HttpService} from '../services';
import {mergeMap, map, catchError} from 'rxjs/operators';
import {of} from 'rxjs';
import {Book, BookDetail} from '../models';
import {
  BookActionTypes,
  GetBooks,
  GetBooksFailure,
  GetBooksSuccess, SearchBooks
} from '../state';
import {GetBookDetail, GetBookDetailSuccess, GetBookDetailFailure} from './books.actions';
import {AppActionTypes, GetBookChapter, GetBookChapterFailure, GetBookChapterSuccess} from '../../state/app.actions';

@Injectable()
export class BooksEffects {
  constructor(
    private actions$: Actions,
    private service: HttpService
  ) {
  }

  @Effect()
  loadBooks$ = this.actions$.pipe(
    ofType(BookActionTypes.BOOK_LIST_REQUEST),
    mergeMap((action: GetBooks) => this.service.getBooks().pipe(
      map((books: Book[]) => new GetBooksSuccess(books)),
      catchError(error => of(new GetBooksFailure(error)))
    ))
  );

  @Effect()
  loadBookDetail$ = this.actions$.pipe(
    ofType(BookActionTypes.BOOK_DETAIL_REQUEST),
    mergeMap((action: GetBookDetail) => this.service.getBookDetail(action.payload).pipe(
      map((books: BookDetail) => new GetBookDetailSuccess(books)),
      catchError(error => of(new GetBookDetailFailure(error)))
    ))
  );

  @Effect()
  loadBookChapters$ = this.actions$.pipe(
    ofType(AppActionTypes.APP_CHAPTER_REQUEST),
    mergeMap((action: GetBookChapter) => this.service.getBookChapters(action.payload).pipe(
      map((chapters: BookDetail[]) => new GetBookChapterSuccess(chapters)),
      catchError(error => of(new GetBookChapterFailure(error)))
    ))
  );

  @Effect()
  loadBookSearch$ = this.actions$.pipe(
    ofType(BookActionTypes.BOOK_SEARCH_REQUEST),
    mergeMap((action: SearchBooks) => this.service.searchBooks(action.payload).pipe(
      map((books: Book[]) => new GetBooksSuccess(books)),
      catchError(error => of(new GetBooksFailure(error)))
    ))
  );
}
