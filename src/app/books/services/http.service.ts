import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Book, BookDetail, BookSearchRequest} from '../models';
import {Observable} from 'rxjs';
import {BookPageRequest} from '../models/book-page.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private url = 'https://demos-aj.ue.r.appspot.com/api/books';
  // private url = 'http://localhost:8000/api/books';

  constructor(private http: HttpClient) {
  }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.url)
      .pipe(
        tap(data => console.log(JSON.stringify(data)))
      );
  }

  getBookDetail({page_number, book_id}: BookPageRequest): Observable<BookDetail> {
    const params = new HttpParams().set('page_number', page_number.toString());
    return this.http.get<BookDetail>(`${this.url}/${book_id}`, {params})
      .pipe(
        tap(data => console.log(data))
      );
  }

  getBookChapters({book_id}: BookPageRequest): Observable<BookDetail[]> {
    return this.http.get<BookDetail[]>(`${this.url}/${book_id}/chapters`)
      .pipe(
        tap(data => console.log(data))
      );
  }

  searchBooks({
                search_term, search_source, search_author, search_publisher,
                search_published_date, search_published_location, search_publication_info
              }: BookSearchRequest): Observable<Book[]> {
    const params = new HttpParams()
      .set('search_term', search_term)
      .set('search_author', String(search_author))
      .set('search_source', String(search_source))
      .set('search_publisher', String(search_publisher))
      .set('search_published_date', String(search_published_date))
      .set('search_published_location', String(search_published_location))
      .set('search_publication_info', String(search_publication_info));

    return this.http.get<Book[]>(this.url, {params})
      .pipe(
        tap(data => console.log(JSON.stringify(data)))
      );
  }
}
